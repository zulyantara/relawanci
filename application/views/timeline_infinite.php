      <link rel = "stylesheet" 
         href = "https://cdnjs.cloudflare.com/ajax/libs/framework7/1.4.2/css/framework7.ios.min.css" />
      <link rel = "stylesheet" 
         href = "https://cdnjs.cloudflare.com/ajax/libs/framework7/1.4.2/css/framework7.ios.colors.min.css" />
         

<nav class="navbar is-fixed-top" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" style="flex-grow: 1; justify-content: center; color: white;" href="https://bulma.io">
      Relawan PMI DKI Jakarta
    </a>
  </div>
</nav>

      <div class = "views">
         <div class = "view view-main">
<div class="pages navbar-through">
  <div data-page = "home" class = "page">
    <div class="page-content infinite-scroll">
    <div class = "column list-block">
    
      <ul>
        <li class = "item-content">
          <div class="item-content card margin-bottom-5">
            <div class="card-content">
              <div class="media hr">
                <div class="media-left">
                  <figure class="image is-48x48">
                    <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
                  </figure>
                </div>
                <div class="media-content">
                  <p class="title is-4">John Smith</p>
                  <p class="subtitle is-6">Posted 17 hours ago</p>
                </div>
              </div>

              <div class="content">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                <a href="#">#css</a> <a href="#">#responsive</a>
              </div>
            </div>
          </div>
        </li>
        <li class = "item-content">
          <div class="card margin-bottom-5">
            <div class="card-content">
              <div class="media hr">
                <div class="media-left">
                  <figure class="image is-48x48">
                    <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
                  </figure>
                </div>
                <div class="media-content">
                  <p class="title is-4">John Smith</p>
                  <p class="subtitle is-6">Posted 17 hours ago</p>
                </div>
              </div>

              <div class="content">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                <a href="#">#css</a> <a href="#">#responsive</a>
              </div>
            </div>
          </div>
        </li>
        <li class = "item-content">
          <div class="card margin-bottom-5">
            <div class="card-content">
              <div class="media hr">
                <div class="media-left">
                  <figure class="image is-48x48">
                    <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
                  </figure>
                </div>
                <div class="media-content">
                  <p class="title is-4">John Smith</p>
                  <p class="subtitle is-6">Posted 17 hours ago</p>
                </div>
              </div>

              <div class="content">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                <a href="#">#css</a> <a href="#">#responsive</a>
              </div>
            </div>
          </div>
        </li>
        <li class = "item-content">
          <div class="card margin-bottom-5">
            <div class="card-content">
              <div class="media hr">
                <div class="media-left">
                  <figure class="image is-48x48">
                    <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
                  </figure>
                </div>
                <div class="media-content">
                  <p class="title is-4">John Smith</p>
                  <p class="subtitle is-6">Posted 17 hours ago</p>
                </div>
              </div>

              <div class="content">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                <a href="#">#css</a> <a href="#">#responsive</a>
              </div>
            </div>
          </div>
        </li>
        <li class = "item-content">
          <div class="card margin-bottom-5">
            <div class="card-content">
              <div class="media hr">
                <div class="media-left">
                  <figure class="image is-48x48">
                    <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
                  </figure>
                </div>
                <div class="media-content">
                  <p class="title is-4">John Smith</p>
                  <p class="subtitle is-6">Posted 17 hours ago</p>
                </div>
              </div>

              <div class="content">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                <a href="#">#css</a> <a href="#">#responsive</a>
              </div>
            </div>
          </div>
        </li>
        <li class = "item-content">
          <div class="card margin-bottom-5">
            <div class="card-content">
              <div class="media hr">
                <div class="media-left">
                  <figure class="image is-48x48">
                    <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
                  </figure>
                </div>
                <div class="media-content">
                  <p class="title is-4">John Smith</p>
                  <p class="subtitle is-6">Posted 17 hours ago</p>
                </div>
              </div>

              <div class="content">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                <a href="#">#css</a> <a href="#">#responsive</a>
              </div>
            </div>
          </div>
        </li>
        <li class = "item-content">
          <div class="card margin-bottom-5">
            <div class="card-content">
              <div class="media hr">
                <div class="media-left">
                  <figure class="image is-48x48">
                    <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
                  </figure>
                </div>
                <div class="media-content">
                  <p class="title is-4">John Smith</p>
                  <p class="subtitle is-6">Posted 17 hours ago</p>
                </div>
              </div>

              <div class="content">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                <a href="#">#css</a> <a href="#">#responsive</a>
              </div>
            </div>
          </div>
        </li>
        <li class = "item-content">
          <div class="card margin-bottom-5">
            <div class="card-content">
              <div class="media hr">
                <div class="media-left">
                  <figure class="image is-48x48">
                    <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
                  </figure>
                </div>
                <div class="media-content">
                  <p class="title is-4">John Smith</p>
                  <p class="subtitle is-6">Posted 17 hours ago</p>
                </div>
              </div>

              <div class="content">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                <a href="#">#css</a> <a href="#">#responsive</a>
              </div>
            </div>
          </div>
        </li>
        
        
      </ul>
      
    </div>
    <div class = "infinite-scroll-preloader">
                        <div class = "preloader"></div>
           </div>
    </div>
  </div>
</div>
  </div>
</div>

<script type = "text/javascript" 
         src = "https://cdnjs.cloudflare.com/ajax/libs/framework7/1.4.2/js/framework7.min.js"></script>
         
      <style>
         .infinite-scroll-preloader {
            margin-top:-20px;
            margin-bottom:10px;
            text-align:center;
         }
         
         .infinite-scroll-preloader .preloader {
            width:34px;
            height:34px;
         }
      </style>
      
      <script>
         var myApp = new Framework7();
         var $$ = Dom7;

         // Loading flag
         var loading = false;

         // Last loaded index
         var lastIndex = $$('.list-block li').length;

         // Max items to load
         var maxItems = 60;

         // Append items per load
         var itemsPerLoad = 20;

         // Attach 'infinite' event handler
         $$('.infinite-scroll').on('infinite', function () {

            // Exit, if loading in progress
            if (loading) return;

            // Set loading flag
            loading = true;

            // Emulate 1s loading
            setTimeout(function () {
               // Reset loading flag
               loading = false;

               if (lastIndex >= maxItems) {
                  // Nothing more to load, detach infinite scroll events to prevent unnecessary loadings
                  myApp.detachInfiniteScroll($$('.infinite-scroll'));
                  
                  // Remove preloader
                  $$('.infinite-scroll-preloader').remove();
                  return;
               }

               // Generate new items HTML
               var html = '';
               for (var i = lastIndex + 1; i <= lastIndex + itemsPerLoad; i++) {
                  html += '<li class = "item-content">'
                            +'<div class="card margin-bottom-5">'
                              +'<div class="card-content">'
                                +'<div class="media hr">'
                                  +'<div class="media-left">'
                                    +'<figure class="image is-48x48">'
                                      +'<img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">'
                                    +'</figure>'
                                  +'</div>'
                                  +'<div class="media-content">'
                                    +'<p class="title is-4">John Smith</p>'
                                    +'<p class="subtitle is-6">Posted 17 hours ago</p>'
                                  +'</div>'
                                +'</div>'

                                +'<div class="content">'
                                  +'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
                                  +'Phasellus nec iaculis mauris. <a>@bulmaio</a>.'
                                  +'<a href="#">#css</a> <a href="#">#responsive</a>'
                                +'</div>'
                              +'</div>'
                            +'</div>';
                          +'</li>';
                  
                  //html += '<li class = "item-content"><div class = "item-inner"><div class = "item-title">Item ' + i + '</div></div></li>';
               }

               // Append new items
               $$('.list-block ul').append(html);

               // Update last loaded index
               lastIndex = $$('.list-block li').length;
            }, 1000);
         });
      </script>