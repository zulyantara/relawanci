<nav class="navbar is-fixed-top" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" style="flex-grow: 1; justify-content: center; color: white;" href="https://bulma.io">
      Relawan PMI DKI Jakarta
    </a>
  </div>
</nav>

<?php

//echo "<pre>";
//print_r($timeline);


?>


<div class="section margin-top-6">
  <div class="columns">
    <div class="column">
    
      <?php
        foreach ($timeline['data'] AS $k => $v) {
      ?>
      <div class="card margin-bottom-5">
        <div class="card-content">
          <div class="media hr">
            <div class="media-left">
              <figure class="image is-48x48">
                <img class="is-rounded" src="<?=$v['user_profile_id']['image']?>" alt="Placeholder image">
              </figure>
            </div>
            <div class="media-content">
              <p class="title is-4"><?=$v['user_profile_id']['name']?></p>
              <p class="subtitle is-6">Posted <?=$v['created_at']?></p>
            </div>
          </div>

          <div class="content">
            <?=$v['title']?>
            
            <br />
            <br />
            
            <?php
              if (strlen($v['media_url']) > 0) {
                if ($v['type'] == 'image') {
                  ?>
                    <img  src="<?=$v['media_url']?>" class="img-responsive" alt="">
                  <?php
                } elseif ($v['type'] == 'video') {
                  ?>
                    <video width="320" height="240" controls>
                      <source src="<?=$v['media_url']?>" type="video/mp4">
                      Your browser does not support the video tag.
                    </video>
                  <?php
                }
              }
            ?>
          </div>
        </div>
      </div>
      <?php
        }
      ?>
    </div>
  </div>
</div>