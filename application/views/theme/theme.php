<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Relawan PMI Jakarta</title>
    <link href="<?php echo base_url('src/fontawesome/css/all.css'); ?>" rel="stylesheet">
    <link rel="stylesheet" href="src/css/mystyles.css">
  </head>
  <body>
    <?php $this->load->view($web_content);?>
  </body>
</html>
