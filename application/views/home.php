<div class="columns" style="background-color: #c62828;">
  <div class="column">
    <section class="section">
      <div class="container has-text-centered">

        <section class="hero">
          <div class="hero-body">
            <div class="container">
              <img src="<?php echo base_url('src/img/logo.png'); ?>" alt="">
              <h1 class="title">
                Relawan PMI
              </h1>
              <h1 class="title">
                Jakarta
              </h1>
            </div>
          </div>
        </section>

      </div>
    </section>
  </div>
</div>

<div class="columns">
  <div class="column has-text-centered">
    <a href="<?php echo site_url('timeline'); ?>" class="button is-medium">
      <span class="icon is-medium">
        <i class="fab fa-facebook"></i>
      </span>
      <span>&nbsp;FACEBOOK</span>
    </a>
    <a href="<?php echo site_url('timeline'); ?>" class="button is-medium">
      <span>
        <i class="fab fa-google"></i>
      </span>
      <span>&nbsp;GOOGLE</span>
    </a>
  </div>

</div>
