<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Timeline extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    
    $data['timeline'] = $this->get_timeline();
    
		$data['web_content'] = 'timeline';
		$this->load->view('theme/theme',$data);
    
  }
  
  public function get_timeline() {
		
		if($_POST){
			//$province_id = $_POST['prov_id'];
		}

		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://relawanpmi.yourapps.id/api/posts?treding=1&page=1",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Authorization: Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjNiYmQyOGVkYzNkMTBiOTI5ZjU3NWEyY2E2ODU0OWZjYTZkODg5OTMiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vcG1pLXNldGlwIiwibmFtZSI6IkwgQmF5dSIsInBpY3R1cmUiOiJodHRwczovL2xoNS5nb29nbGV1c2VyY29udGVudC5jb20vLVlXbVFrdlhJb1hBL0FBQUFBQUFBQUFJL0FBQUFBQUFBQUJFL0R3MmNydU1jSnA4L3M5Ni1jL3Bob3RvLmpwZyIsImF1ZCI6InBtaS1zZXRpcCIsImF1dGhfdGltZSI6MTU0OTM3OTQ5MiwidXNlcl9pZCI6InNzWDI4aXdyNUFSaDFDTmR4NmQxbVhPYXFLbDEiLCJzdWIiOiJzc1gyOGl3cjVBUmgxQ05keDZkMW1YT2FxS2wxIiwiaWF0IjoxNTUwNzYxNTYwLCJleHAiOjE1NTA3NjUxNjAsImVtYWlsIjoibGVzLmJheXVhamlAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZ29vZ2xlLmNvbSI6WyIxMDE4NDQxOTk3ODc0NzIyMzk5NzIiXSwiZW1haWwiOlsibGVzLmJheXVhamlAZ21haWwuY29tIl19LCJzaWduX2luX3Byb3ZpZGVyIjoiZ29vZ2xlLmNvbSJ9fQ.S1m97WXHS1_KPvyupYmb92r9BJO45d9nLIQOJ4-Uk0waLJxY0oGE9tsNbWbFoXDd6p70SK05ZiuE-N3BipjiHp0ETwVczR25O5rq_TP_C9nPmpGzSs8EKxUpWnFcWp1gQKLSIF3nUuOmKmfmqviVI5YFoi07yOdlM_cBwJS_FpDdqPleYKcwL9owVpxbR0q0BXP5BYa_vFJ6WdPU7Huj8HogeBMUqegFujoq9ggapJz9vxp0nMMtZ8VBrm5pRpGWVhIuOOfU_7ARVr_siQjEp5iVo3fyvGWZHcnBP9I4GTJCy0XdyPdc5iVLEt3i6V3xeFr7G2ah_q2D0y4WdV6-BQ"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			//echo $response;
		}

		$data = json_decode($response, true);
    
    return $data;
		
	}
}